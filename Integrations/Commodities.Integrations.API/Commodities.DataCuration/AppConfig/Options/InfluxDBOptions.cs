﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.DataCuration.AppConfig.Options
{
    public class InfluxDBOptions
    {
        public string Bucket { get; set; }
        public string Org { get; set; }
        public string Url { get; set; }
        public string Token { get; set; }
    }
}
