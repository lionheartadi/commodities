﻿using Commodities.Common.Events.Messages;
using Commodities.DataCuration.Entities.CommoditiesAPI;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client.Writes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.DataCuration.Entities.InfluxDB
{
    public static class IndluxDBLatestCommoditiesFactory
    {
        public static List<InfluxDBLatestCommodity> GetCommoditiesAPIMeasurementsFromMessage(QueueMessage<LatestHttpResponse> queueMessage)
        {
            var commodities = new List<InfluxDBLatestCommodity>();

            if(queueMessage == null || queueMessage.Payload == null || queueMessage.Payload.Data == null || !queueMessage.Payload.Data.Success)
                return commodities;

            commodities.Add(new InfluxDBLatestCommodity(nameof(queueMessage.Payload.Data.Rates.Coffee), queueMessage.Payload.Data.Rates.Coffee, queueMessage.Payload.Data.Date));
            commodities.Add(new InfluxDBLatestCommodity(nameof(queueMessage.Payload.Data.Rates.Brentoil), queueMessage.Payload.Data.Rates.Brentoil, queueMessage.Payload.Data.Date));
            commodities.Add(new InfluxDBLatestCommodity(nameof(queueMessage.Payload.Data.Rates.Corn), queueMessage.Payload.Data.Rates.Corn, queueMessage.Payload.Data.Date));
            commodities.Add(new InfluxDBLatestCommodity(nameof(queueMessage.Payload.Data.Rates.Rice), queueMessage.Payload.Data.Rates.Rice, queueMessage.Payload.Data.Date));
            commodities.Add(new InfluxDBLatestCommodity(nameof(queueMessage.Payload.Data.Rates.Soybean), queueMessage.Payload.Data.Rates.Soybean, queueMessage.Payload.Data.Date));
            commodities.Add(new InfluxDBLatestCommodity(nameof(queueMessage.Payload.Data.Rates.Wheat), queueMessage.Payload.Data.Rates.Wheat, queueMessage.Payload.Data.Date));

            return commodities;
        }

        public static List<PointData> GetCommoditiesAPIDataPointsFromMessage(QueueMessage<LatestHttpResponse> queueMessage)
        {
            var commodities = new List<PointData>();

            if (queueMessage == null || queueMessage.Payload == null || queueMessage.Payload.Data == null || !queueMessage.Payload.Data.Success)
                return commodities;

            commodities.Add(PointData.Measurement("latestCommodity")
                    .Tag("rate", nameof(queueMessage.Payload.Data.Rates.Coffee))
                    .Field("value", 1 / queueMessage.Payload.Data.Rates.Coffee)
                    .Timestamp(DateTime.UtcNow, WritePrecision.Ns));

            commodities.Add(PointData.Measurement("latestCommodity")
                    .Tag("rate", nameof(queueMessage.Payload.Data.Rates.Brentoil))
                    .Field("value", 1 / queueMessage.Payload.Data.Rates.Brentoil)
                    .Timestamp(DateTime.UtcNow, WritePrecision.Ns));

            commodities.Add(PointData.Measurement("latestCommodity")
                    .Tag("rate", nameof(queueMessage.Payload.Data.Rates.Corn))
                    .Field("value", 1 / queueMessage.Payload.Data.Rates.Corn)
                    .Timestamp(DateTime.UtcNow, WritePrecision.Ns));

            commodities.Add(PointData.Measurement("latestCommodity")
                    .Tag("rate", nameof(queueMessage.Payload.Data.Rates.Rice))
                    .Field("value", 1 / queueMessage.Payload.Data.Rates.Rice)
                    .Timestamp(DateTime.UtcNow, WritePrecision.Ns));

            commodities.Add(PointData.Measurement("latestCommodity")
                    .Tag("rate", nameof(queueMessage.Payload.Data.Rates.Soybean))
                    .Field("value", 1 / queueMessage.Payload.Data.Rates.Soybean)
                    .Timestamp(DateTime.UtcNow, WritePrecision.Ns));

            commodities.Add(PointData.Measurement("latestCommodity")
                    .Tag("rate", nameof(queueMessage.Payload.Data.Rates.Wheat))
                    .Field("value", 1 / queueMessage.Payload.Data.Rates.Wheat)
                    .Timestamp(DateTime.UtcNow, WritePrecision.Ns));

            return commodities;
        }



        
    }
}
