﻿using InfluxDB.Client.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.DataCuration.Entities.InfluxDB
{
    [Measurement("latestCommodity")]
    public class InfluxDBLatestCommodity
    {
        public InfluxDBLatestCommodity(string rateName, double rate, DateTime dateTime)
        {
            RateName = rateName;
            Rate = (float)rate;
            Time = dateTime;
        }

        [Column("rate", IsTag = true)]
        public string RateName { get; private set; }
        [Column("value")]
        public float Rate { get; private set; }

        [Column("time", IsTimestamp = true)]
        public DateTime Time { get; private set; }
    }
}
