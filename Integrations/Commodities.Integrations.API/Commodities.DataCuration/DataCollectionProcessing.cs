using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Commodities.Common.Events.Messages;
using Commodities.DataCuration.AppConfig.Options;
using Commodities.DataCuration.Entities.CommoditiesAPI;
using Commodities.DataCuration.Entities.InfluxDB;
using InfluxDB.Client;
using InfluxDB.Client.Api.Domain;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Commodities.DataCuration
{
    public class DataCollectionProcessing
    {
        private readonly ILogger _logger;
        private readonly IOptions<InfluxDBOptions> _influxDBOptions;

        public DataCollectionProcessing(ILoggerFactory loggerFactory, IOptions<InfluxDBOptions> influxDBOptions)
        {
            _logger = loggerFactory.CreateLogger<DataCollectionProcessing>();
            _influxDBOptions = influxDBOptions;

        }

        [Function("DataCollectionProcessingTrigger")]
        public async Task Run([ServiceBusTrigger("%DataCollectionQueue%", Connection = "ServiceBusConnection")] string queueItem)
        {
            // TODO: Log to telemery API
            _logger.LogInformation($"C# ServiceBus queue trigger function processed message: {queueItem}");

            var message = JsonSerializer.Deserialize<QueueMessage<LatestHttpResponse>>(queueItem);
            using var client = InfluxDBClientFactory.Create(_influxDBOptions.Value.Url, _influxDBOptions.Value.Token);

            using (var writeApi = client.GetWriteApi())
            {
                //writeApi.WriteMeasurements<InfluxDBLatestCommodity>(_influxDBOptions.Value.Bucket,_influxDBOptions.Value.Org, WritePrecision.Ns, IndluxDBLatestCommoditiesFactory.GetCommoditiesAPIMeasurementsFromMessage(message));
                writeApi.WritePoints(_influxDBOptions.Value.Bucket, _influxDBOptions.Value.Org, IndluxDBLatestCommoditiesFactory.GetCommoditiesAPIDataPointsFromMessage(message));
            }
        }
    }
}
