using Commodities.DataCuration.AppConfig.Options;
using Microsoft.Azure.Functions.Worker.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;
using System.Threading.Tasks;

namespace Commodities.DataCuration
{
    public class Program
    {
        public static void Main()
        {
            var host = new HostBuilder()
                .ConfigureFunctionsWorkerDefaults()
                .ConfigureAppConfiguration((hostContext, configApp) =>
                {
                    configApp.AddJsonFile(Path.Combine(hostContext.HostingEnvironment.ContentRootPath, $"AppConfig\\appsettings.json"), optional: true, reloadOnChange: false);
                    configApp.AddJsonFile(Path.Combine(hostContext.HostingEnvironment.ContentRootPath, $"AppConfig\\appsettings.{hostContext.HostingEnvironment.EnvironmentName.ToLowerInvariant()}.json"), optional: true, reloadOnChange: false);
                    configApp.AddJsonFile(Path.Combine(hostContext.HostingEnvironment.ContentRootPath, $"local.settings.json"), optional: true, reloadOnChange: true);
                })
                 .ConfigureServices((hostContext, services) =>
                 {
                     services.AddOptions<InfluxDBOptions>()
                .Configure<IConfiguration>((settings, configuration) => { configuration.Bind("InfluxDB", settings); });
                 })
                .Build();

            host.Run();
        }
    }
}