﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.Common.DataSources
{
    public class DataOperationStatus
    {
        public DataOperationStatus(bool isSuccess, string? message = null, long bytesWritten = 0)
        {
            IsSuccess = isSuccess;
            Message = message ?? String.Empty;
            BytesWritten = bytesWritten;
        }

        public bool IsSuccess { get; private set; } = false;
        public string Message { get; private  set; } = string.Empty;

        public long BytesWritten { get; private set; } = 0;
    }
}
