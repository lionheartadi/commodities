﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Azure.Identity;
using Azure.Messaging.ServiceBus;
using Commodities.Common.AppConfig.Options;
using Commodities.Common.Events.Messages;
using Microsoft.Extensions.Options;

namespace Commodities.Common.Events
{
    public class QueueManager : IEventManager
    {
        private readonly string _fullyQualifiedNamespace = string.Empty;
        private readonly string _queue = string.Empty;

        private readonly ServiceBusClient _client;
        private readonly ServiceBusSender _sender;

        public QueueManager(IOptions<CommoditiesServiceBusOptions> configuration)
        {
            _fullyQualifiedNamespace = configuration.Value.FullyQualifiedNamespace;
            _queue = configuration.Value.DataCollectionQueue;

            var environemntType = Environment.GetEnvironmentVariable("ENVIRONMENT", EnvironmentVariableTarget.Process);
            if (string.IsNullOrEmpty(_fullyQualifiedNamespace))
                throw new Exception("FullyQualifiedNamespace configuration is not defined for the Commodities ServiceBus connection.");

            if (environemntType == "development")
            {
                _client = new ServiceBusClient(configuration.Value.ServiceBusConnection);
            }
            else
            {
                _client = new ServiceBusClient(_fullyQualifiedNamespace, new DefaultAzureCredential());
            }

            _sender = _client.CreateSender(_queue);

            // TODO: Telemery logging
        }

        public async Task<EventOperationStatus> PublishAsync<T>(QueueMessage<T> message)
        {
            // TODO: Telemery logging
            await _sender.SendMessageAsync(new ServiceBusMessage(JsonSerializer.Serialize(message)));

            return new EventOperationStatus(true);
        }
    }
}
