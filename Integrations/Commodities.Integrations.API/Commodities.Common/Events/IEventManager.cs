﻿using Commodities.Common.Events.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.Common.Events
{
    public interface IEventManager
    {
        Task<EventOperationStatus> PublishAsync<T>(QueueMessage<T> message);
    }
}
