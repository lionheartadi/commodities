﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.Common.Events.Messages
{
    public class QueueMessage<T>
    {
        public QueueMessage(T payload, Guid id, Guid correlationId)
        {
            Payload = payload;
            Id = id;
            CorrelationId = correlationId;
        }

        public T Payload { get; private set; }
        public Guid Id { get; private set; }
        public Guid CorrelationId { get; private set; }
    }
}
