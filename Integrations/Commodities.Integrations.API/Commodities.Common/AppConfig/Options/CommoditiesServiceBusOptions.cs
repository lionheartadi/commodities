﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.Common.AppConfig.Options
{
    public class CommoditiesServiceBusOptions
    {
        public string FullyQualifiedNamespace { get; set; }
        public string DataCollectionQueue { get; set; }

        public string ServiceBusConnection { get; set; }
    }
}
