﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.Common.AppConfig.Options
{
    public class AzureDataLakeOptions
    {
        public string AzureStorageUrl { get; set; }
        public string AccountName { get; set; }
        public string AccountKey { get; set; }
    }
}
