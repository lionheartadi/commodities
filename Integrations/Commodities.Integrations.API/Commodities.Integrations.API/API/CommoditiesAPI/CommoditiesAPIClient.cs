﻿using Commodities.DataCollection.AppConfig.Options;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Commodities.DataCollection.API.CommoditiesAPI
{
    public class CommoditiesAPIClient : ICommoditiesAPIClient
    {
        private readonly IOptions<CommoditiesAPIOptions> _commoditiesAPIconfiguration;
        private readonly HttpClient _httpClient;

        public CommoditiesAPIClient(IOptions<CommoditiesAPIOptions> commoditiesAPIconfiguration, HttpClient httpClient)
        {
            _commoditiesAPIconfiguration = commoditiesAPIconfiguration;
            _httpClient = new HttpClient();
        }

        public async Task<LatestHttpResponse> GetCommoditesLatestData(IDictionary<string, string> requestQueryParams)
        {
            requestQueryParams.Add("access_key", _commoditiesAPIconfiguration.Value.AccessKey);

            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;


            request.RequestUri = new Uri(QueryHelpers.AddQueryString(_commoditiesAPIconfiguration.Value.ApiUrl, requestQueryParams));
            
            var response = await _httpClient.SendAsync(request);

            // TODO: Better error response and logging
            if (response.StatusCode != HttpStatusCode.OK)
                return null;

            return await JsonSerializer.DeserializeAsync<LatestHttpResponse>(await response.Content.ReadAsStreamAsync(), new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
        }
    }
}
