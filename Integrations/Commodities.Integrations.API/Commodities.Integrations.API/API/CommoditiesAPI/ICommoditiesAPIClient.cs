﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.DataCollection.API.CommoditiesAPI
{
    public interface ICommoditiesAPIClient
    {
        Task<LatestHttpResponse> GetCommoditesLatestData(IDictionary<string, string> requestQueryParams);
    }
}
