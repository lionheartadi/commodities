using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Commodities.Common.DataSources;
using Commodities.Common.DataSources.ObjectStore;
using Commodities.Common.Events;
using Commodities.DataCollection.AppConfig.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;

namespace Commodities.DataCollection.API.CommoditiesAPI
{
    public class CommoditiesAPI
    {
        private readonly ILogger<CommoditiesAPI> _logger;
        
        private readonly IDataStoreService _dataStoreService;
        private readonly IEventManager _queueManager;
        private readonly ICommoditiesAPIClient _commoditiesAPIClient;

        public CommoditiesAPI(ILogger<CommoditiesAPI> log, IDataStoreService dataStoreService, IEventManager queueManager, ICommoditiesAPIClient commoditiesAPIClient)
        {
            _logger = log;
            _dataStoreService = dataStoreService;
            _dataStoreService.SetRootDirectory("bronze/raw/commoditiesapi");
            _queueManager = queueManager;
            _commoditiesAPIClient = commoditiesAPIClient;
        }

        [FunctionName("CommoditiesAPI-Latest")]
        [OpenApiOperation(operationId: "CommoditiesAPI-Latest", tags: new[] { "CommoditiesAPI" })]
        [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
        [OpenApiParameter(name: "base", In = ParameterLocation.Query, Required = true, Type = typeof(string), Description = "The **base** parameter is to specify in which currency to get the monetary values. More info: https://www.commodities-api.com/symbols")]
        [OpenApiParameter(name: "symbols", In = ParameterLocation.Query, Required = true, Type = typeof(string), Description = "The **symbols** parameter is to specify in which commodities you want data from. More info: https://www.commodities-api.com/symbols")]
        [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: "text/plain", bodyType: typeof(DataOperationStatus), Description = "The OK response")]
        public async Task<IActionResult> Latest(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = "CommoditiesAPI/Latest")] HttpRequest req, ExecutionContext context)
        {
            _logger.LogInformation("C# HTTP trigger function CommoditiesAPI-Latest processed a request.");

            var requestParams = req.GetQueryParameterDictionary();

            if (!(requestParams.ContainsKey("base") && requestParams.ContainsKey("symbols") && requestParams.Count == 2))
                return new BadRequestObjectResult("Missing base or symbols query parameter or you have non acceptable params");

            // TODO: Better error and logging management
            var apiData = await _commoditiesAPIClient.GetCommoditesLatestData(requestParams);
            await _queueManager.PublishAsync(new Common.Events.Messages.QueueMessage<LatestHttpResponse>(apiData, Guid.NewGuid(), Guid.NewGuid()));

            var dataOperationResult = await _dataStoreService.WriteDataDump(JsonSerializer.Serialize(apiData), DateTime.UtcNow, "latest_data");

            return new OkObjectResult(dataOperationResult);
        }
    }
}

