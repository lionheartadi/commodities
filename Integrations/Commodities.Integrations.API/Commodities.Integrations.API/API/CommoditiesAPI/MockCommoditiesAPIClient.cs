﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.DataCollection.API.CommoditiesAPI
{
    public class MockCommoditiesAPIClient : ICommoditiesAPIClient
    {
        /// <summary>
        /// Example data:
        /// {
        ///        ""data"": {
        ///            ""success"": true,
        ///            ""timestamp"": 1644224160,
        ///            ""date"": ""2022-02-07"",
        ///            ""base"": ""EUR"",
        ///            ""rates"": {
        ///                ""BRENTOIL"": 0.012349472043316807,
        ///                ""COFFEE"": 0.4729834687587303,
        ///                ""CORN"": 0.0018279366397875101,
        ///                ""RICE"": 0.0746999152282195,
        ///                ""SOYBEAN"": 0.07302852299812213,
        ///                ""WHEAT"": 0.004310798742318975,
        ///                ""WTIOIL"": 0.012539640391526712,
        ///                ""XAG"": 19.683931308622736,
        ///                ""XAU"": 1580.362659879792,
        ///                ""XPD"": 1954.1310718426541,
        ///                ""XPT"": 888.548875899278,
        ///                ""XRH"": 14723.985159646198
        ///            },
        ///            ""unit"": ""per barrel for Oil, per ounce for Metals.Per 10 metric tons for Crude Palm Oil, Per MMBtu for Natural gas, Per Gallon for Ethanol.Per metric ton, per lb or per bushel for Agriculture""
        ///        }
        ///    }
        /// </summary>
        /// <param name="requestQueryParams"></param>
        /// <returns></returns>
        public async Task<LatestHttpResponse> GetCommoditesLatestData(IDictionary<string, string> requestQueryParams)
        {
            Faker faker = new Faker("en");

            LatestHttpResponse.Rates rates = new LatestHttpResponse.Rates(faker.Random.Double(0.01, 0.02),
                faker.Random.Double(0.4, 0.5),
                faker.Random.Double(0.001, 0.002),
                faker.Random.Double(0.07, 0.08),
                faker.Random.Double(0.07, 0.08),
                faker.Random.Double(0.004, 0.005),
                faker.Random.Double(0.01, 0.002),
                faker.Random.Double(19.0, 20.0),
                faker.Random.Double(1500, 1600),
                faker.Random.Double(1900, 2000),
                faker.Random.Double(800, 900),
                faker.Random.Double(14000, 15000));

            LatestHttpResponse.MainData data = new LatestHttpResponse.MainData(true, DateTime.UtcNow.Ticks, DateTime.UtcNow.Date, "EUR", rates, faker.Random.String(256));
            LatestHttpResponse latestHttpResponse = new LatestHttpResponse(data);

            return latestHttpResponse;
        }
    }
}
