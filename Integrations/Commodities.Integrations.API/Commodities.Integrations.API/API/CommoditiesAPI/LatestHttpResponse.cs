﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Commodities.DataCollection.API.CommoditiesAPI
{
    public class LatestHttpResponse
    {
        [JsonConstructor]
        public LatestHttpResponse(MainData data)
        {
            Data = data;
        }

        public MainData Data { get; private set; }

        public class MainData
        {
            [JsonConstructor]
            public MainData(bool success, long timestamp, DateTime date, string @base, Rates rates, string unit)
            {
                Success = success;
                Timestamp = timestamp;
                Date = date;
                Base = @base;
                Rates = rates;
                Unit = unit;
            }

            public bool Success { get; private set; }
            public long Timestamp { get; private set; }

            public DateTime Date { get; private set; }

            public string Base { get; private set; }

            public Rates Rates { get; private set; }

            public string Unit { get; private set; }
        }

        public class Rates
        {
            [JsonConstructor]
            public Rates(double brentoil, double coffee, double corn, double rice, double soybean, double wheat, double wtioil, double xag, double xau, double xpd, double xpt, double xrh)
            {
                Brentoil = brentoil;
                Coffee = coffee;
                Corn = corn;
                Rice = rice;
                Soybean = soybean;
                Wheat = wheat;
                Wtioil = wtioil;
                Xag = xag;
                Xau = xau;
                Xpd = xpd;
                Xpt = xpt;
                Xrh = xrh;
            }

            [JsonPropertyName("BRENTOIL")]
            public double Brentoil { get; private set; }
            [JsonPropertyName("COFFEE")]
            public double Coffee { get; private set; }
            [JsonPropertyName("CORN")]
            public double Corn { get; private set; }
            [JsonPropertyName("RICE")]
            public double Rice { get; private set; }
            [JsonPropertyName("SOYBEAN")]
            public double Soybean { get; private set; }
            [JsonPropertyName("WHEAT")]
            public double Wheat { get; private set; }
            [JsonPropertyName("WTIOIL")]
            public double Wtioil { get; private set; }
            [JsonPropertyName("XAG")]
            public double Xag { get; private set; }
            [JsonPropertyName("XAU")]
            public double Xau { get; private set; }
            [JsonPropertyName("XPD")]
            public double Xpd { get; private set; }
            [JsonPropertyName("XPT")]
            public double Xpt { get; private set; }
            [JsonPropertyName("XRH")]
            public double Xrh { get; private set; }
        }
    }
}
