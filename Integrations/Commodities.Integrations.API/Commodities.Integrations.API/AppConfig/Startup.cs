﻿
using Commodities.Common.AppConfig.Options;
using Commodities.Common.DataSources.ObjectStore;
using Commodities.Common.Events;
using Commodities.DataCollection.API.CommoditiesAPI;
using Commodities.DataCollection.AppConfig.Options;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Reflection;

[assembly: FunctionsStartup(typeof(Commodities.Integrations.API.AppConfig.Startup))]
namespace Commodities.Integrations.API.AppConfig
{
    public class Startup : FunctionsStartup
    {
        private IConfiguration _config;
        private string _environemnt = String.Empty;

        public IConfiguration Configuration
        {
            get
            {
                return _config;
            }
        }

        public Startup()
        { }

        public Startup(IConfiguration configuration)
        {
            _config = configuration;
        }

        public override void Configure(IFunctionsHostBuilder builder)
        {
            _environemnt = GetEnvironmentVariable("ENVIRONMENT");
            if (string.IsNullOrEmpty(_environemnt))
            {
                _environemnt = "development";
            }

            var services = builder.Services;

            // Configure services
            // Configure dependency injection
            var assembly = Assembly.GetExecutingAssembly();
            services.AddLogging();

            services.AddOptions<AzureDataLakeOptions>()
                .Configure<IConfiguration>((settings, configuration) => { configuration.Bind("AzureDataLake", settings); });

            services.AddOptions<CommoditiesAPIOptions>()
                .Configure<IConfiguration>((settings, configuration) => { configuration.Bind("CommoditiesAPI", settings); });

            services.AddOptions<CommoditiesServiceBusOptions>()
                .Configure<IConfiguration>((settings, configuration) => { configuration.Bind("CommoditiesServiceBus", settings); });

            ConfigureServices(builder.Services);
        }

        public override void ConfigureAppConfiguration(IFunctionsConfigurationBuilder builder)
        {
            Configure(builder);
        }

        private static string GetEnvironmentVariable(string name)
        {
            return Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IDataStoreService, AzureDataLakeService>();
            services.AddScoped<IEventManager, QueueManager>();

            if(_environemnt.Contains("development"))
            {
                services.AddScoped<ICommoditiesAPIClient, MockCommoditiesAPIClient>();
            }
            else
            {
                services.AddScoped<ICommoditiesAPIClient, CommoditiesAPIClient>();
            }
        }

        public virtual void Configure(IFunctionsConfigurationBuilder configurationBuilder)
        {
            // Configure application
            FunctionsHostBuilderContext context = configurationBuilder.GetContext();

            _config = configurationBuilder.ConfigurationBuilder
                .AddJsonFile(Path.Combine(context.ApplicationRootPath, $"AppConfig\\appsettings.json"), optional: true, reloadOnChange: false)
                .AddJsonFile(Path.Combine(context.ApplicationRootPath, $"AppConfig\\appsettings.{context.EnvironmentName.ToLowerInvariant()}.json"), optional: true, reloadOnChange: false)
                .AddJsonFile(Path.Combine(context.ApplicationRootPath, $"local.settings.json"), optional: true, reloadOnChange: true)
                .AddEnvironmentVariables().Build();
        }
    }
}
