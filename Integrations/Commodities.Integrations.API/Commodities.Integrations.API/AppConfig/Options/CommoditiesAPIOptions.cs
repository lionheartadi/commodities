﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commodities.DataCollection.AppConfig.Options
{
    public class CommoditiesAPIOptions
    {
        public string ApiUrl { get; set; }
        public string AccessKey { get; set; }
    }
}
